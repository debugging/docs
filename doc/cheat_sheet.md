> **Disclaimer**: Use the following page at your own risk.  This Cheat Sheet
> is for the GitLab Support Team to use.

## Contents

* [Rails](#rails)
* [Command Line](#command-line)
* [Projects](#projects)
* [Repository](#repository)
* [Imports / Exports](#imports-exports)
* [Users](#users)
* [LDAP](#ldap)
* [Routes](#routes)
* [MRs](#mrs)
* [CI](#ci)
* [License](#license)
* [Unicorn](#unicorn)
* [Sidekiq](#sidekiq)
* [Redis](#redis)
* [LFS](#lfs)

## Rails

### Enable debug logging on rails console

```ruby
Rails.logger.level = 0
```

### Enable debug logging for ActiveRecord (db issues)

```ruby
ActiveRecord::Base.logger = Logger.new(STDOUT)
```

### Find specific methods for an object

```ruby
Array.methods.select { |m| m.to_s.include? "sing" }
Array.methods.grep(/sing/)
```

### Query an object 

```ruby
o = Object.where('attribute like ?', 'ex')
```

### Rails console history

```ruby
puts Readline::HISTORY.to_a
```

### Profile a page

```ruby
# the Rails way (replace URL)
logger = Logger.new(STDOUT)
admin_token = User.find_by_username('ADMIN_USERNAME').personal_access_tokens.first.token
app.get('URL/?private_token=#{admin_token}')
```

###  Using the GitLab profiler inside console (used as of 10.5)
```ruby
logger = Logger.new(STDOUT)
admin = User.find_by_username('ADMIN_USERNAME')
Gitlab::Profiler.profile('URL', logger: logger, user: admin)
```

## Command Line

### Check the GitLab version fast

```bash
grep gitlab /opt/gitlab/version-manifest.txt
```

### Debugging SSH

```bash
GIT_SSH_COMMAND="ssh -vvv" git clone <repository>
```

### Debugging over HTTPS

```bash
GIT_CURL_VERBOSE=1 GIT_TRACE=1 git clone <repository>
```

## Projects

### Find a project

```ruby
project = Project.find_by_full_path('')
```

### Clear a project's cache

```ruby
ProjectCacheWorker.perform_async(project.id)
```

### Expire the .exists? cache

```ruby
project.repository.expire_exists_cache
```

### Make all projects private

```ruby
Project.update_all(visibility_level: 0)
```

### Destroy a project 

```ruby
p = Project.find_by_full_path('')
u = User.find_by_username('')
ProjectDestroyWorker.perform_async(p.id, u.id, {}) 
# or ProjectDestroyWorker.new.perform(p.id, u.id, {}) 
```

### Remove fork relationship manually

```ruby
p = Project.find_by_full_path('')
u = User.find_by_username('')
::Projects::UnlinkForkService.new(p, u).execute
```

### Make a project read-only (can only be done in the console)

```ruby
Make a project read-only
project.repository_read_only = true; project.save

# OR
project.update!(repository_read_only: true)
```

## Imports / Exports

### Fail an import

```ruby
Fail an import
# Find the project and get the error
p = Project.find_by_full_path('<username-or-group>/<project-name>')

p.import_error

# To finish the import
p.import_finish
```

### Rename imported repository

A customer had a backup restore that failed on a single repository. It created the project with an empty repository. They were able to successfully restore it to a dev instance. From there, they created an export and imported it into a new project. They did not want the project under the new name. We were able to transfer the newly imported project to the empty project.

Move the new repository to the empty repository

```bash
mv /var/opt/gitlab/git-data/repositories/<group>/<new-project> /var/opt/gitlab/git-data/repositories/<group>/<empty-project>
```

Make sure the permissions are correct

```bash
chown -R git:git <path-to-directory>.git
```

Clear the cache

```bash
sudo gitlab-rake cache:clear
```

## Repository

### Search sequence of pushes on a repository 

https://gitlab.slack.com/archives/C4Y5DRKLK/p1523984649000229

```ruby
p = Project.find_with_namespace('u/p')
p.events.code_push.first(20).each do |e|
  printf "%-20.20s %8s...%8s (%s)\n", e.data[:ref], e.data[:before], e.data[:after], e.author.try(:username)
end
```

GitLab 9.5 and above:

```ruby
p = Project.find_with_namespace('u/p')
p.events.code_push.first(20).each do |e|
  printf "%-20.20s %8s...%8s (%s)\n", e.push_event_payload[:ref], e.push_event_payload[:commit_from], e.push_event_payload[:commit_to], e.author.try(:username)
end
```

If you're here, chances are you have a customer saying "GitLab lost my commit!". [This StackOverflow article](https://stackoverflow.com/questions/13468027/the-mystery-of-the-missing-commit-across-merges) describes how you can end up in this state without a force push. If you look at the above output for the target branch, you will see a discontinuity in the from/to commits as you step through the output. Each new push should be "from" the "to" sha of the previous push. When this discontinuity happens, you will see two pushes with the same "from" sha.

## Users

### Block

```ruby
User.find_by_username().block!
```

### Unblock

```ruby
User.find_by_username().active
```

### Skip reconfirmation

```ruby
user = User.find_by_username ''
user.skip_reconfirmation!
```

### Get an admin token

```ruby
# Get the first admin's first access token
User.where(admin:true).first.personal_access_tokens.first.token

# Get the first admin's private token (no longer works on 10.2+)
User.where(admin:true).private_token
```

### Active users & Historical users

```ruby
# Active users on the instance, now
User.active.count

# The historical max on the instance as of the past year
::HistoricalData.max_historical_user_count
```

## LDAP

### LDAP commands in the rails console

```ruby
### *** TIP: use the rails runner to avoid entering the rails console in the first place.
### This is great when only a single command (such as a UserSync or GroupSync)
### is needed.

# Get debug output
Rails.logger.level = Logger::DEBUG

# Run a UserSync (normally performed once a day)
LdapSyncWorker.new.perform

# Run a GroupSync for all groups (9.3-)
LdapGroupSyncWorker.new.perform

# Run a GroupSync for all groups (9.3+)
LdapAllGroupsSyncWorker.new.perform

# Run a GroupSync for a single group (10.6-)
group = Group.find_by(name: 'my_gitlab_group')
EE::Gitlab::LDAP::Sync::Group.execute_all_providers(group)

# Run a GroupSync for a single group (10.6+)
group = Group.find_by(name: 'my_gitlab_group')
EE::Gitlab::Auth::LDAP::Sync::Group.execute_all_providers(group)

# Query an LDAP group directly (10.6-)
adapter = Gitlab::LDAP::Adapter.new('ldapmain') # If `main` is the LDAP provider
ldap_group = EE::Gitlab::LDAP::Group.find_by_cn('group_cn_here', adapter)
ldap_group.member_dns
ldap_group.member_uids

# Query an LDAP group directly (10.6+)
adapter = Gitlab::Auth::LDAP::Adapter.new('ldapmain') # If `main` is the LDAP provider
ldap_group = EE::Gitlab::Auth::LDAP::Group.find_by_cn('group_cn_here', adapter)
ldap_group.member_dns
ldap_group.member_uids
```

## Routes

### Remove redirecting routes
https://gitlab.com/gitlab-org/gitlab-ce/issues/41758#note_54828133
```ruby
path = 'foo'
conflicting_permanent_redirects = RedirectRoute.permanent.matching_path_and_descendants(path)

# Check that conflicting_permanent_redirects is as expected
conflicting_permanent_redirects.destroy_all
```

## MRs 

### Find MR

```ruby
m = project.merge_requests.find_by(iid: <IID>) 
m = MergeRequest.find_by_title('NEEDS UNIQUE TITLE!!!')
```

### Close a merge request properly (if merged but still marked as open)

```ruby
p = Project.find_by_full_path('')
m = project.merge_requests.find_by(iid: ) 
u = User.find_by_username('')
MergeRequests::PostMergeService.new(p, u).execute(m)
```

### Rebase manually

```ruby
p = Project.find_by_full_path('')
m = project.merge_requests.find_by(iid: ) 
u = User.find_by_username('')
MergeRequests::RebaseService.new(m.target_project, u).execute(m) 
```

### Try CI service

```ruby
p = Project.find_by_full_path('')
m = project.merge_requests.find_by(iid: ) 
m.project.try(:ci_service)
```
 
## CI 

### Cancel stuck pending pipelines
https://gitlab.com/gitlab-com/support-forum/issues/2449#note_41929707

```ruby
Ci::Pipeline.where(project_id: p.id).where(status: 'pending').count
Ci::Pipeline.where(project_id: p.id).where(status: 'pending').each {|p| p.cancel}
Ci::Pipeline.where(project_id: p.id).where(status: 'pending').count
```

### Manually modify runner minutes

```ruby
Namespace.find_by_full_path("user/proj").namespace_statistics.update(shared_runners_seconds: 27360)
```

### Remove artifacts more than a week old

```ruby
### SELECTING THE BUILDS TO CLEAR
# For a single project:
project = Project.find_by_full_path('') 
builds_with_artifacts =  project.builds.with_artifacts

# Instance-wide:
builds_with_artifacts = Ci::Build.with_artifacts

### CLEAR THEM OUT
builds_to_clear = builds_with_artifacts.where("finished_at < ?", 1.week.ago)
builds_to_clear.each do |build|
  build.artifacts_expire_at = Time.now
  build.erase_artifacts!
end
```

### Find reason failure (for when build trace is empty) (Introduced in 10.3.0)

https://gitlab.com/gitlab-org/gitlab-ce/issues/41111 

```ruby
build = Ci::Build.find(78420)

build.failure_reason

build.dependencies.each do |d| { puts "status: #{d.status}, finished at: #{d.finished_at}, 
completed: #{d.complete?}, artifacts_expired: #{d.artifacts_expired?}, erased: #{d.erased?}" }
```
### Disable strict artifact checking (Introduced in GitLab 10.3.0)

https://docs.gitlab.com/ee/administration/job_artifacts.html#validation-for-dependencies

```ruby
Feature.enable('ci_disable_validates_dependencies')
```

## License

### See license plan name (since v9.3.0-ee)

```ruby
License.first.plan
```

### Check if a project feature is available in a project
Features listed in https://gitlab.com/gitlab-org/gitlab-ee/blob/master/ee/app/models/license.rb

```ruby
p = Project.find_by_full_path('<group>/<project>')
p.feature_available?(:jira_dev_panel_integration)
```
### Add a license through the console

```ruby
key = "<key>"
license = License.new(data: key)
license.save
License.current # check to make sure it applied
```

### Remove CI traces older than 6 months
```ruby
current_user = User.find_by_email('cindy@gitlap.com')
Ci::Build.where("finished_at < ?", 6.months.ago.to_date).each {|b| puts b.id; b.erase(erased_by: current_user) if b.erasable?};nil
```

## Unicorn
From #91083
### Poll unicorn requests by seconds

```ruby
require 'rubygems'
require 'unicorn'

# Usage for this program
def usage
  puts "ruby unicorn_status.rb <path to unix socket> <poll interval in seconds>"
  puts "Polls the given Unix socket every interval in seconds. Will not allow you to drop below 3 second poll intervals."
  puts "Example: /opt/gitlab/embedded/bin/ruby poll_unicorn.rb /var/opt/gitlab/gitlab-rails/sockets/gitlab.socket 10"
end

# Look for required args. Throw usage and exit if they don't exist.
if ARGV.count < 2
  usage
  exit 1
end

# Get the socket and threshold values.
socket = ARGV[0]
threshold = (ARGV[1]).to_i

# Check threshold - is it less than 3? If so, set to 3 seconds. Safety first!
if threshold.to_i < 3
  threshold = 3
end

# Check - does that socket exist?
unless File.exist?(socket)
  puts "Socket file not found: #{socket}"
  exit 1
end

# Poll the given socket every THRESHOLD seconds as specified above.
puts "Running infinite loop. Use CTRL+C to exit."
puts "------------------------------------------"
loop do
  Raindrops::Linux.unix_listener_stats([socket]).each do |addr, stats|
    puts DateTime.now.to_s + " Active: " + stats.active.to_s + " Queued: " + stats.queued.to_s
  end
  sleep threshold
end
```

## Sidekiq

### Kill a worker's Sidekiq jobs

```ruby
queue = Sidekiq::Queue.new('repository_import')
queue.each { |job| job.delete if <condition>}
```

### Enable debug logging of Sidekiq

```ruby
gitlab_rails['env'] = {
  'SIDEKIQ_LOG_ARGUMENTS' => "1"
}
```

Then `gitlab-ctl reconfigure; gitlab-ctl restart sidekiq`.  The Sidekiq logs will now include additional data for troubleshooting.

## Redis

### Connect to redis (omnibus)

```shell
/opt/gitlab/embedded/bin/redis-cli -s /var/opt/gitlab/redis/redis.socket 
```

### Connect to redis (HA)

```shell
/opt/gitlab/embedded/bin/redis-cli -h <host ip> -a <password>
```


## LFS

### Get info about LFS objects and associated project

```
o=LfsObject.find_by(oid: "<oid>")
p=Project.find(LfsObjectsProject.find_by_lfs_object_id(o.id).project_id)
```

You can then delete these records from the database with:

```
LfsObjectsProject.find_by_lfs_object_id(o.id).destroy
o.destroy
```

which you would also want to combine with deleting the LFS file in the LFS storage area on disk. It remains to be seen exactly how or whether the deletion is useful, however.

